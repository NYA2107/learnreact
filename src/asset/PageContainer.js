import React from 'react';
import styled from 'styled-components'
import {Button, Modal, Input} from 'semantic-ui-react'
import ReactTable from 'react-table-v6'



let Container = styled.div`
  width:100%;
  height:100vh;
  padding:10px;
  display:grid;
  grid-template-rows:auto 1fr;
  overflow:auto;
`
class InputSPP extends React.Component{


  render(){
    return(
      <Container>
        {this.props.children}
      </Container>  
    )
  }
}


export default InputSPP;
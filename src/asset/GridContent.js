import React from 'react'
import styled from 'styled-components'

let Container = styled.div`
	grid-column:${props => props.grid_c || '1/2'};
	grid-row:${props => props.grid_r || '1/2'};
`

export default ({className, grid_c, grid_r, children, style}) => {
	return(
		<Container style={style} className={className} grid_c={grid_c} grid_r={grid_r}>
			{children}
		</Container>
	)
}




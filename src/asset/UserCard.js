import React from 'react'
import {Card, Image} from 'semantic-ui-react'

class UserCard extends React.Component{
	constructor(props){
		super(props)

	}

	clicked=()=>{
		this.props.onCardClick(this.props)
	}


	render(){
		if(this.props.active){
			return(
				<Card onClick={this.clicked} style={{border:'2px solid green'}}>
					<Card.Content style={{padding:'3px', paddingTop:'8px'}}>
						<Image
							src={this.props.imgSrc}
							size="mini"
							floated="left"
						/>
						<Card.Header style={{fontSize:'12pt'}}>
							{this.props.name} <span style={{color:`${(this.props.date2 - this.props.date1 > 2?'orange':'green')}`}}> {this.props.date1}/{this.props.date2} Bulan </span>
						</Card.Header>
						<Card.Meta>
							{this.props.number} / {this.props.classRoom}
						</Card.Meta>
					</Card.Content>
				</Card>
			)
		}else{
			return(
				<Card onClick={this.clicked}>
					<Card.Content style={{padding:'3px', paddingTop:'8px'}}>
						<Image
							src={this.props.imgSrc}
							size="mini"
							floated="left"
						/>
						<Card.Header style={{fontSize:'12pt'}}>
							{this.props.name} <span style={{color:`${(this.props.date2 - this.props.date1 > 2?'orange':'green')}`}}> {this.props.date1}/{this.props.date2} Bulan </span>
						</Card.Header>
						<Card.Meta>
							{this.props.number} / {this.props.classRoom}
						</Card.Meta>
					</Card.Content>
				</Card>
			)
		}
	}
}

export default UserCard
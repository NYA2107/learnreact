import React from 'react'
import UserCard from './UserCard'
import {Card} from 'semantic-ui-react'

class CardList extends React.Component{
	constructor(props){
		super(props)
		this.state = {
			active:''
		}
	}

	onCardClick=(data)=>{
		this.props.onCardClick(data)
	}

	render(){
		return(
			<Card.Group>
				{this.props.data.map((v, i)=>
					<UserCard
					onCardClick={this.onCardClick}
					active={v.id == this.props.activeCard}
					key = {i}
					id = {v.id}
					name={v.name} 
					date1={v.date1} 
					date2={v.date2} 
					number={v.number} 
					classRoom={v.classRoom}
					imgSrc={v.imgSrc}
					jenisPembayaran={v.jenisPembayaran}
					jumlahPembayaran={v.jumlahPembayaran}
					waktuPembayaran={v.waktuPembayaran}
					/>)}

			</Card.Group>	
		)
	}
}

export default CardList
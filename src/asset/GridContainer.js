import React from 'react'
import styled from 'styled-components'

let Container = styled.div`
	display:grid;
	grid-template-columns:${props => props.grid_t_c?props.grid_t_c:'1fr'};
	grid-template-rows:${props => props.grid_t_r?props.grid_t_r:'1fr'};
`

export default ({className, grid_t_c, grid_t_r, children, style}) => {
	return(
		<Container style={style} className={className} grid_t_c={grid_t_c} grid_t_r={grid_t_r}>
			{children}
		</Container>
	)
}




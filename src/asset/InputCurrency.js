import React from 'react'
import {Input} from 'semantic-ui-react'
import _ from 'lodash'

class InputCurrency extends React.Component{
	constructor(props){
		super(props)
		this.state = {
			value:''
		}

		this.form = new Intl.NumberFormat('id', {
		  style: 'currency',
		  currency: 'IDR',
		  minimumFractionDigits: 2
		})
	}

	componentDidMount(){
		console.log(this.props.value)
		if(this.props.value){
			this.setState({value:this.changeToCurr(this.props.value)[0]})	
		}
		
	}

	componentDidUpdate(prevState, prevProps, snap){
		if(this.props.value !== prevProps.value){
			if(this.props.value){
				this.setState({value:this.changeToCurr(this.props.value)[0]})	
			}else{
				this.setState({value:this.props.value})
			}
			
		}
	}

	changeToCurr(input){
		input = input.toString()
		let temp = ""
		let realNum = undefined
		let arrNum = input.match(/\d/g)
		// let tiga = Math.floor(input.length / 3)
		let batasDepan = arrNum.length % 3
		// let arrNum = Array.from(numbers.toString()).map(Number);
		// console.log(arrNum)
		
		let sisaBelakang = _.drop(arrNum.slice(),batasDepan)
		let sisaDepan = _.dropRight(arrNum.slice(),sisaBelakang.length)
		let bagiTiga = _.chunk(sisaBelakang.slice(),3)


		console.log(sisaDepan, "Depan", sisaBelakang.length)
		console.log(bagiTiga, "Belakang", batasDepan)
		sisaDepan.forEach((v)=>{
			temp = temp + v
		})
		if(sisaDepan.length != 0 && arrNum.length>3){
			temp = temp + "."
		}
		bagiTiga.forEach((v,i)=>{
			v.forEach(n=>{
				temp = temp + n
			})
			if(i < bagiTiga.length-1){
				temp = temp + "."	
			}
		})
		realNum = arrNum.toString().replace(/,/g,"")
		return [temp, realNum]
	}

	onInputChange=(e)=>{
		let temp = ""
		let realNum = undefined
		if(e.target.value){
			let hasil = this.changeToCurr(e.target.value)
			temp = hasil[0]
			realNum = hasil[1]
		}
		
		this.setState({value:temp})
		if(this.props.onValueChange){
			this.props.onValueChange({
				number:realNum,
				text:temp
			})	
		}
		
	}

	

	render(){
		return(
			<Input
				onChange={this.onInputChange}
	    		size="mini"
			    label={{ basic: true, content: 'Rp' }}
			    labelPosition='left'
			    placeholder='Jumlah...'
			    value={this.state.value}
			    disabled={this.props.disabled}
			/>
		)
	}
}

export default InputCurrency
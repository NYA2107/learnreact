import React from 'react'
import styled from 'styled-components'
import {Divider} from 'semantic-ui-react'

const LeftHeader = styled.div`
	grid-column:1/2;
`

const RightHeader = styled.div`
	grid-column:2/3;
`

const Header = styled.div`
	width:100%;
	height:auto;
	display:grid;
	grid-template-columns:1fr auto;
	grid-template-rows:auto;
`
const DividerGrid = styled(Divider)`
	grid-column:1/3;
`


export class Left extends React.Component{
	constructor(props){
		super(props)
	}

	render(){
		return(
			<LeftHeader className={this.props.className}>
				{this.props.children}	
			</LeftHeader>
		)
	}
}

export class Right extends React.Component{
	constructor(props){
		super(props)
	}

	render(){
		return(
			<RightHeader>
				{this.props.children}	
			</RightHeader>
		)
	}
}

export class HeaderLayout extends React.Component{
	constructor(props){
		super(props)
	}

	render(){
		return(
			<Header>
				{this.props.children}
				<DividerGrid/>
			</Header>
		)
	}
} 


import React from 'react'
import ReactTable from 'react-table-v6'

export default (props) => {
	const data = [
		{
		    name: 'Tanner Linsley',
		    age: 26,
		    friend: {
		      name: 'Jason Maurer',
		      age: 23,
		    }
	  	},
	  	{
		    name: 'Tanner Linsley',
		    age: 26,
		    friend: {
		      name: 'Jason Maurer',
		      age: 23,
		    }
	  	},
	  	{
		    name: 'Tanner Linsley',
		    age: 26,
		    friend: {
		      name: 'Jason Maurer',
		      age: 23,
		    }
	  	}
  	]

	const columns = [
		{
			Header: 'Name',
			accessor: 'name' // String-based value accessors!
		}, 
		{
			Header: 'Age',
			accessor: 'age',
			Cell: props => <span style={{color:'blue'}}>{props.value}</span> // Custom cell components!
		}, 
		{
			id: 'friendName', // Required because our accessor is not a string
			Header: 'Friend Name',
			accessor: 'friend.name' // Custom value accessors!
		}, 
		{
			Header: props => <h2>Friend Age</h2>, // Custom header components!
			accessor: 'friend.age'
		}
	]

	return (
		<ReactTable data={data} columns={columns} />
	)
}
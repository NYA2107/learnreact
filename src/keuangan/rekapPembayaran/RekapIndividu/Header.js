import React from 'react'
import styled from 'styled-components'
import {HeaderLayout , Left, Right} from '../../../asset/HeaderContainer'
import {Header, Button, Icon, Dropdown} from 'semantic-ui-react'

let LeftFlex = styled(Left)`
	display:flex;
`

class HeaderRekap extends React.Component{

	render(){
		return(
			<HeaderLayout>
				<LeftFlex>
					<div>
						<Button size='mini' basic color="blue"><Icon name='arrow left'/></Button>
					</div>
					<Header as='h2' style={{marginTop:'3px'}}>
						Rekapitulasi Pembayaran : Ana Sihombing
						<Header.Subheader>
							<Dropdown 
								style={{color:'blue'}}
								defaultValue='2017/2018/1'
    							options={[
    								{
    									key:'17.18.1',
    									value:'2017/2018/1',
    									text:'Tahun Ajaran 2017/2018 Semester 1'
    								},
    								{
    									key:'17.18.2',
    									value:'2017/2018/2',
    									text:'Tahun Ajaran 2017/2018 Semester 2'	
    								}
    							]}
							/>
						</Header.Subheader>
					</Header>
				</LeftFlex>
				<Right>
					<Button basic color='blue' attached='left'><Icon name='print'/>Cetak</Button>
    				<Button basic color='blue' attached='right'><Icon name='download'/>Download</Button>
    				<Button style={{marginLeft:'10px'}} primary><Icon name='cc mastercard'/>Pembayaran</Button>
				</Right>
			</HeaderLayout>
		)
	}
}

export default HeaderRekap
import React from 'react'
import styled from 'styled-components'
import Body from '../../../asset/BodyContainer'
import {Header, Button, Icon, Input, Divider, Image, Segment, Grid, Radio, Form, Dropdown} from 'semantic-ui-react'
import ReactTable from 'react-table-v6'
import _ from 'lodash'
import moment from 'moment'


let Container = styled.div`
	width:100%;
	display:flex;
`
let Card = styled.div`
	width:${props => props.width||'100%'};
	padding:10px;
`
let BorderedCard = styled(Card)`
	border:1px solid rgba(180,180,180,0.5);
	border-radius:2px;
	margin-left:5px;
	margin-right:5px;
`

class BodyRekap extends React.Component{


	constructor(props){
		super(props)

		this.state = {
			filterWaktu:'all',
			filterJenis:'all',
			filterData:[],
			editJumlah:'',
			editCatatan:'',
			waktuOptions:[],
			edit:'',
			data:[
				{
					waktu:moment(),
					petugas:'Ahmad Salim',
					jenis:'SPP',
					jumlah:{
						id:'id1',
						val:300000
					},
					catatan:{
						id:'id1',
						val:'Bulan Agustus 2019, Tahun Ajaran 2018/2019'	
					},
					id:'id1'
				},
				{
					waktu:moment(),
					petugas:'Ahmad Fauzan',
					jenis:'Kas Wajib',
					jumlah:{
						id:'id2',
						val:300000
					},
					catatan:{
						id:'id2',
						val:'Bulan Februari 2019, Tahun Ajaran 2018/2019'	
					},
					id:'id2'
				},
				{
					waktu:moment().subtract(100, 'days'),
					petugas:'Ahmad Salim',
					jenis:'SPP',
					jumlah:{
						id:'id3',
						val:300000
					},
					catatan:{
						id:'id3',
						val:'Bulan Januari 2019, Tahun Ajaran 2018/2019'	
					},
					id:'id3'
				}
			]
		}

		this.columns = [
			{
				Header:'Waktu',
				accessor:'waktu',
				Cell:props=>{
					return(
						<span>{props.value.format('lll')}</span>
					)
				},
				minWidth:50
			},
			{
				Header:'Petugas',
				accessor:'petugas',
				minWidth:50
			},
			{
				Header:'Janis',
				accessor:'jenis',
				minWidth:30
			},
			{
				Header:'Jumlah',
				accessor:'jumlah',
				minWidth:50,
				Cell:props=>{
					let isEdit = this.state.edit == props.value.id
					return(isEdit?
						<Input size='mini' value={this.state.editJumlah} onChange={this.onEditJumlahChange} style={{minWidth:'30px'}}/>
						:
						<span>{props.value.val}</span>
					)
				}
			},
			{
				Header:'Catatan',
				accessor:'catatan',
				Cell:props=>{
					let isEdit = this.state.edit == props.value.id
					return(isEdit?
						<Input size='mini' value={this.state.editCatatan} onChange={this.onEditCatatanChange} style={{minWidth:'300px'}}/>
						:
						<span>{props.value.val}</span>
					)
				}
			},
			{
				Header:'Aksi',
				accessor:'id',
				width:110,
				Cell:props=>{
					let v = props.value
					let isEdit = this.state.edit == v
					return((isEdit)?
						<div style={{textAlign:'center'}}>
							<Icon style={{cursor:'pointer', color:'blue'}} onClick={()=>this.setState({edit:''})} name='delete' />
							<Icon style={{cursor:'pointer', color:'green'}} onClick={this.onEditSave} name='checkmark' />
						</div>
						:
						<div style={{textAlign:'center'}}>
							<Icon style={{cursor:'pointer', color:'red'}} onClick={()=>this.onDelete(v)} name='trash alternate outline' />
							<Icon style={{cursor:'pointer', color:'blue'}} onClick={()=>this.onEdit(v)} name='edit' />
							<Icon style={{cursor:'pointer', color:'blue'}} name='print' />
						</div>
					)
				}
			}
		]
	}

	componentDidMount(){
		let tempWaktu = this.getAllWaktu()
		tempWaktu.unshift({key:'all',value:'all',text:'Semua'})
		this.setState({filterData:this.state.data, waktuOptions:tempWaktu})
		
	}

	onDelete=(value)=>{
		let temp = this.state.data
		let removed = _.remove(temp, (v)=>{
			return v.id == value
		})
		let tempWaktu = this.getAllWaktu()
		tempWaktu.unshift({key:'all',value:'all',text:'Semua'})
		this.setState({data:temp, filterData:this.filterChange(), waktuOptions:tempWaktu})
	}

	onEdit=async (value)=>{
		console.log(this.state.data)
		if(this.state.edit == ''){
			let index = _.findIndex(this.state.data,(v)=>{
				return v.id == value
			})
			let jumlah = this.state.data[index].jumlah.val
			let catatan = this.state.data[index].catatan.val
			await this.setState({editCatatan:catatan,editJumlah:jumlah ,edit:value})
		}
		
	}

	onEditJumlahChange=(e,{value})=>{
		this.setState({editJumlah:value})
	}
	onEditCatatanChange=(e,{value})=>{
		this.setState({editCatatan:value})
	}

	onEditSave=async ()=>{
		console.log(this.state.edit)
		console.log(this.state.editCatatan)
		console.log(this.state.editJumlah)
		let tempData = this.state.data
		let newData = tempData.map((v,i)=>{
			if(v.id == this.state.edit){
				let temp = Object.assign(v, {jumlah:{
					id:v.id,
					val:this.state.editJumlah
				}})
				temp = Object.assign(v,{catatan:{
					id:v.id,
					val:this.state.editCatatan
				}})
				return temp
			}
			return v
		})

		await this.setState({edit:'', data:newData, filterData:this.filterChange()})
	}

	getAllWaktu(){
		let temp = this.state.data.map((v,i)=>{
			return ({
				key: i,
				text: v.waktu.format('lll'),
				value: v.waktu.format('lll')
			})
		})
		
		let uni = _.uniqBy(temp,'value')
		return uni
	}

	filterChange(){
		let temp = this.state.data
		let isAllWaktu = this.state.filterWaktu == 'all'
		let isAllJenis = this.state.filterJenis == 'all'
		console.log(isAllWaktu,'UHUY')
		console.log(isAllJenis,'aaa', this.state.filterJenis)
		let filterArr = _.filter(temp, (v)=>{
			if(isAllWaktu && !isAllJenis){
				return v.jenis == this.state.filterJenis	
			}else if(isAllJenis && !isAllWaktu){
				return v.waktu.format('lll') == this.state.filterWaktu
			}else if(!isAllWaktu && !isAllJenis){
				return v.waktu.format('lll') == this.state.filterWaktu && v.jenis == this.state.filterJenis
			}else{
				return true
			}
			
		})
		return filterArr
	}

	onWaktuChange= async (e, {value})=>{
		await this.setState({filterWaktu : value})
		await this.setState({filterData : this.filterChange()})
	}

	onJenisChange= async (e, {value})=>{
		await this.setState({filterJenis : value})
		await this.setState({filterData : this.filterChange()})
	}

	

	render(){
		return(
			<Body>
				<Container>
					<Card style={{width:'auto'}}>
						<Image
							src='https://react.semantic-ui.com/images/avatar/large/molly.png'
							size="medium"
							floated="center"
						/>
					</Card>
					<Card>
						<Header color="blue" as='h4'>Nama/NIS</Header>
						<Header as='h3' style={{marginTop:'10px'}}>Elaenora Josephine/171810009</Header>
					</Card>
					<Card style={{width:'auto', minWidth:'130px'}}>
						<Header color="blue" as='h4'>Kelas</Header>
						<Header as='h3' style={{marginTop:'10px'}}>X-C(MIPA)</Header>
					</Card>
					<Card style={{width:'auto', minWidth:'200px'}}>
						<Header color="blue" as='h4'>Wali Kelas</Header>
						<Header as='h3' style={{marginTop:'10px'}}>Yani Kusmawati</Header>
					</Card>
					<Card>
						<Header color="blue" as='h4'>Konseling</Header>
						<Header as='h3' style={{marginTop:'10px'}}>Isacc Muhammad</Header>
					</Card>
				</Container>
				<Divider/>
				<Container>
					<BorderedCard>
						<Header color="blue" as='h4'>Tunggakan</Header>
						<Divider/>
						<Header as='h1' style={{marginTop:'10px', color:'red'}}>
							-1000000
							<Header.Subheader>
							(Rp0)
							</Header.Subheader>
						</Header>
					</BorderedCard>
					<BorderedCard>
						<Header color="blue" as='h4'>Kas Wajib</Header>
						<Divider/>
						<Header as='h1' style={{marginTop:'10px', color:'orange'}}>
							500000
							<Header.Subheader>
							(Rp1000000)
							</Header.Subheader>
						</Header>
					</BorderedCard>
					<BorderedCard>
						<Header color="blue" as='h4'>Kas Lorem</Header>
						<Divider/>
						<Header as='h1' style={{marginTop:'10px', color:'green'}}>
							500000
							<Header.Subheader>
							(Min Rp500000)
							</Header.Subheader>
						</Header>
					</BorderedCard>
					<BorderedCard>
						<Header color="blue" as='h4'>SPP</Header>
						<Divider/>
						<Container>
							<Header as='h1' style={{marginTop:'0px'}}>
								6
								<Header.Subheader>
								Bulan
								</Header.Subheader>
							</Header>
							<Header as='h1' style={{marginTop:'0px'}}>
								*
							</Header>
							<Header as='h1' style={{marginTop:'0px',marginLeft:'10px'}}>
								300000
								<Header.Subheader>
								Rp/Bulan
								</Header.Subheader>
							</Header>
							<Header as='h1' style={{marginTop:'0px',marginLeft:'20px',color:'orange'}}>
								1200000
								<Header.Subheader>
								(Rp.1.800.000)
								</Header.Subheader>
							</Header>
						</Container>
					</BorderedCard>
				</Container>
				<Container>
					<Card width='300px'>
						<Header as='h4' color="blue">
							Waktu
						</Header>
						<Dropdown
							defaultValue="all"
						    fluid
						    value={this.state.filterWaktu}
						    onChange={this.onWaktuChange}
						    selection
						    options={this.state.waktuOptions}
						/>
					</Card>
					<Card width='300px'>
						<Header as='h4' color="blue">
							Jenis
						</Header>
						<Dropdown
							defaultValue="all"
						    fluid
						    value={this.state.filterJenis}
						    onChange={this.onJenisChange}
						    selection
						    options={[
						    	{
						    		key:'all',
						    		value:'all',
						    		text:'Semua'
						    	},
						    	{
						    		key:'spp',
						    		value:'SPP',
						    		text:'SPP'
						    	},
						    	{
						    		key:'kasWajib',
						    		value:'Kas Wajib',
						    		text:'Kas Wajib'
						    	}
						    ]}
						/>
					</Card>
					
				</Container>
				<ReactTable defaultPageSize={5} data={this.state.filterData} columns={this.columns}/>
			</Body>
		)
	}
}

export default BodyRekap
import React from 'react'
import styled from 'styled-components'
import Body from '../../../asset/BodyContainer'
import {Header, Button, Icon, Input, Divider, Image, Segment, Grid, Radio, Form, Dropdown} from 'semantic-ui-react'
import ReactTable from 'react-table-v6'
import _ from 'lodash'


let Container = styled.div`
	width:100%;
	display:flex;
`
let Card = styled.div`
	width:${props => props.width||'100%'};
	padding:10px;
`

class BodyRekap extends React.Component{


	constructor(props){
		super(props)

		this.state = {
			filterMurid:'',
			filterGender:'all',
			filterData:[],
			data : [
				{
					no:1,
					murid:{
						img:'https://react.semantic-ui.com/images/avatar/large/elliot.jpg',
						name:'John Doe'
					},
					nis:'171810009',
					tunggakan:-1000000,
					kasWajib:500000,
					kasLorem:125000,
					spp:125000,
					gender:'Laki-Laki'
				},
				{
					no:2,
					murid:{
						img:'https://react.semantic-ui.com/images/avatar/large/molly.png',
						name:'Eleanora Josephine'
					},
					nis:'171810019',
					tunggakan:0,
					kasWajib:1000000,
					kasLorem:125000,
					spp:125000,
					gender:'Perempuan'
				},
				{
					no:3,
					murid:{
						img:'https://react.semantic-ui.com/images/avatar/large/jenny.jpg',
						name:'Ristu Saptono'
					},
					nis:'171810019',
					tunggakan:0,
					kasWajib:1000000,
					kasLorem:125000,
					spp:125000,
					gender:'Laki-Laki'
				}
			]
		}

		this.columns = [
			{
				Header:'No.',
				accessor:'no',
				width:35
			},
			{
				Header:'Murid',
				accessor:'murid',
				Cell:props=>{
					return(<div>
						<Image
							src={props.value.img}
							size="mini"
							floated="left"
						/>
						{props.value.name}
					</div>)	
				}
			},
			{
				Header:'NIS',
				accessor:'nis',
				width:100
			},
			{
				Header:'Pembayaran',
				columns:[
					{
						Header:'Tunggakan',
						accessor:'tunggakan',
						Cell:props=>{
							let color = (props.value < 0?'red':'green')
							return(<span style={{color:color}}>{props.value}</span>)
						},
						minWidth:50
					},
					{
						Header:'Kas Wajib',
						accessor:'kasWajib',
						Cell:props=>{
							let color = (props.value < 1000000?'red':'green')
							return(<span style={{color:color}}>{props.value}</span>)
						},
						minWidth:50
					},
					{
						Header:'Kas Lorem',
						accessor:'kasLorem',
						Cell:props=>{
							let color = (props.value < 125000?'red':'green')
							return(<span style={{color:color}}>{props.value}</span>)
						},
						minWidth:50
					},
					{
						Header:'SPP',
						accessor:'spp',
						Cell:props=>{
							let color = (props.value < 125000?'red':'green')
							return(<span style={{color:color}}>{props.value}</span>)
						},
						minWidth:50
					}
				]
			},
			{
				Header:'Aksi',
				accessor:'nis',
				width:60,
				Cell:props=>{
					return(<div style={{textAlign:'center'}}>
						<Icon style={{cursor:'pointer', color:'blue'}} name='angle right' />
					</div>)
				}
			}
		]
	}

	componentDidMount(){
		this.setState({filterData:this.state.data})
	}

	filterChange(){
		let temp = this.state.data
		let isAllGender = this.state.filterGender === 'all'
		let filterArr = _.filter(temp ,(v,i)=>{
			if(isAllGender){
				return v.murid.name.includes(this.state.filterMurid) || v.nis.includes(this.state.filterMurid)
			}else{
				return v.gender == this.state.filterGender && (v.murid.name.includes(this.state.filterMurid) || v.nis.includes(this.state.filterMurid))
			}
		})
		filterArr.map((v,i)=>{
			return Object.assign(v, {no:i+1})
		})
		return filterArr
	}

	onMuridChange= async (e, {value})=>{
		await this.setState({filterMurid : value})
		await this.setState({filterData : this.filterChange()})
	}

	onGenderChange= async (e, {value})=>{
		await this.setState({filterGender : value})
		await this.setState({filterData : this.filterChange()})
	}

	render(){
		return(
			<Body>
				<Container>
					<Card>
						<Header color="blue" as='h4'>Kelas</Header>
						<Header as='h3' style={{marginTop:'10px'}}>X-MIPA-1(MIPA)</Header>
					</Card>
					<Card>
						<Header color="blue" as='h4'>Wali Kelas</Header>
						<Header as='h3' style={{marginTop:'10px'}}>Ana Permata</Header>
					</Card>
					<Card>
						<Header color="blue" as='h4'>Kelas</Header>
						<Header as='h3' style={{marginTop:'10px'}}>Yani Kusmawati</Header>
					</Card>
					<Card>
						<Header color="blue" as='h4'>Jumlah Murid</Header>
						<Header as='h3' style={{marginTop:'10px'}}>40</Header>
					</Card>
				</Container>
				<Divider/>
				<Container>
					<Card>
						<Header as='h4' color="blue">
							Murid
						</Header>
						<Input icon='search' value={this.state.filterMurid} onChange={this.onMuridChange} iconPosition="left" placeholder='Cari Nama/NIS Murid' />
					</Card>
					<Card>
						<Header as='h4' color="blue">
							Jenis Kelamin
						</Header>
						<Dropdown
							defaultValue="all"
						    fluid
						    value={this.state.filterGender}
						    onChange={this.onGenderChange}
						    selection
						    options={[
						    	{
						    		key:'all',
						    		value:'all',
						    		text:'Semua'
						    	},
						    	{
						    		key:'laki',
						    		value:'Laki-Laki',
						    		text:'Laki-Laki'
						    	},
						    	{
						    		key:'perem',
						    		value:'Perempuan',
						    		text:'Perempuan'
						    	}
						    ]}
						/>
					</Card>
					<Card style={{width:'1000px'}}>
					</Card>
				</Container>
				<ReactTable defaultPageSize={10} data={this.state.filterData} columns={this.columns}/>
			</Body>
		)
	}
}

export default BodyRekap
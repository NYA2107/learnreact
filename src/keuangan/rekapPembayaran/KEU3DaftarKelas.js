import React from 'react';
import HeaderC from './Header'
import Body from './Body'
import styled from 'styled-components'
import {Button, Modal, Input} from 'semantic-ui-react'
import ReactTable from 'react-table-v6'
import InputCurrency from '../../asset/InputCurrency'
import PageContainer from '../../asset/PageContainer'


class KEU3 extends React.Component{

  render(){
    return(
      <PageContainer>
        <HeaderC/>
        <Body/>
      </PageContainer>
    )
  }
}


export default KEU3;
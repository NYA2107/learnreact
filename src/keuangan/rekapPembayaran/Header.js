import React from 'react'
import styled from 'styled-components'
import {HeaderLayout , Left, Right} from '../../asset/HeaderContainer'
import {Header, Button, Icon, Dropdown} from 'semantic-ui-react'


class HeaderRekap extends React.Component{

	render(){
		return(
			<HeaderLayout>
				<Left>
					<Header as='h2'>
						Rekapitulasi Pembayaran : Daftar Kelas
						<Header.Subheader>
							<Dropdown 
								defaultValue='2017/2018/1'
    							options={[
    								{
    									key:'17.18.1',
    									value:'2017/2018/1',
    									text:'Tahun Ajaran 2017/2018 Semester 1'
    								},
    								{
    									key:'17.18.2',
    									value:'2017/2018/2',
    									text:'Tahun Ajaran 2017/2018 Semester 2'	
    								}
    							]}
							/>
						</Header.Subheader>
					</Header>
				</Left>
			</HeaderLayout>
		)
	}
}

export default HeaderRekap
import React from 'react'
import styled from 'styled-components'
import Body from '../../asset/BodyContainer'
import {Header, Button, Icon, Input, Divider,Image, Segment, Grid, Radio, Form, Dropdown} from 'semantic-ui-react'
import GridContainer from '../../asset/GridContainer'
import GridContent from '../../asset/GridContent'
import CardList from '../../asset/CardList'
import InputCurrency from '../../asset/InputCurrency'
import ReactTable from 'react-table-v6'
import _ from 'lodash'


let Container = styled.div`
	width:100%;
	display:flex;
`
let Card = styled.div`
	width:${props => props.width||'100%'};
	padding:10px;
`

class BodyRekap extends React.Component{


	constructor(props){
		super(props)

		this.state = {
			data : this.users,
			dataFilter : [],
			today:true,
			modalOpen:true,
			filterMurid:'',
			filterWali:'',
			filterTingkat:'all',
			filterProgram:'all',
			filterData:[],
			data:[
				{
					kelas:'XI-MIPA-1',
					program:'MIPA',
					waliKelas:'Ana',
					konseling:'Ini Sihimbng',
					tunggakan:'10//31',
					kasWajib:'20/31',
					kasLorem:'20/31',
					spp:'20/31',
					tingkat:'XI',
					id:'1'
				},
				{
					kelas:'XII-MIPA-2',
					program:'MIPA',
					waliKelas:'Balijan',
					konseling:'Ini Sihimbng',
					tunggakan:'30/30',
					kasWajib:'30/30',
					kasLorem:'20/31',
					spp:'30/30',
					tingkat:'XII',
					id:'2'		
				},
				{
					kelas:'X-Bahasa-3',
					program:'Bahasa',
					waliKelas:'Ana',
					konseling:'Ini Sihimbng',
					tunggakan:'10//31',
					kasWajib:'20/31',
					kasLorem:'20/31',
					spp:'20/31',
					tingkat:'X',
					id:'3'
				}
			]
		}
		this.columns = [
			{
				Header:props=><span style={{height:'100px'}}>Kelas</span>,
				accessor:'kelas'
			},
			{
				Header: 'Program',
				accessor:'program'
			},
			{
				Header:'Wali Kelas',
				accessor:'waliKelas'
			},
			{
				Header:'Konseling',
				accessor:'konseling'
			},
			{
				Header:'Jumlah Murid Sudah Lunas',
				columns:[
					{
						Header:'Tunggakan',
						accessor:'tunggakan',
						width:100
					},
					{
						Header:'Kas Wajib',
						accessor:'kasWajib',
						width:100
					},
					{
						Header:'Kas Lorem',
						accessor:'kasLorem',
						width:100
					},
					{
						Header:'SPP',
						accessor:'spp',
						width:60
					}
				]
			},{
				Header:'Aksi',
				accessor:'id',
				width:60,
				Cell:props=>{
					return(<div style={{textAlign:'center'}}>
						<Icon style={{cursor:'pointer', color:'blue'}} name='angle right' />
					</div>)
				}
			},

		]
	}

	componentDidMount(){
		this.setState({filterData:this.state.data})
	}

	filterAll(){
		let temp = this.state.data
		let isSemuaTingkat = this.state.filterTingkat === 'all'
		let isSemuaProgram = this.state.filterProgram === 'all'
		let filterArr = _.filter(temp, (v)=>{
			if(isSemuaTingkat && isSemuaProgram){
				return v.waliKelas.includes(this.state.filterWali)	
			}else if(isSemuaTingkat){
				return v.waliKelas.includes(this.state.filterWali) && v.program == this.state.filterProgram
			}else if(isSemuaProgram){
				return v.waliKelas.includes(this.state.filterWali) && v.tingkat == this.state.filterTingkat
			}else{
				return v.waliKelas.includes(this.state.filterWali) && v.tingkat == this.state.filterTingkat && v.program == this.state.filterProgram
			}
			
		})
		return filterArr
	}

	filterMuridChange = async (e, {value}) =>{
		await this.setState({filterMurid:value})
		await this.setState({filterData:this.filterAll()})
		
	}
	filterWaliChange = async (e, {value}) =>{
		await this.setState({filterWali:value})
		await this.setState({filterData:this.filterAll()})
	}
	filterTingkatChange = async (e, {value}) =>{
		await this.setState({filterTingkat:value})
		await this.setState({filterData:this.filterAll()})
	}
	filterProgramChange = async (e, {value}) =>{
		await this.setState({filterProgram:value})
		await this.setState({filterData:this.filterAll()})
	}


	render(){
		console.log(this.state)
		return(
			<Body>
				<Container>
					<Card width="300px">
						<Header as="h4" color="blue">Wali Kelas</Header>
						<Input icon='search' onChange={this.filterWaliChange} value={this.state.filterWali} iconPosition="left" placeholder='Cari Wali Kelas' />
					</Card>
					<Card width="300px">
						<Header as="h4" color="blue">Tingkat</Header>
						<Dropdown
							defaultValue="all"
						    placeholder='Tingkat'
						    fluid
						    selection
						    onChange={this.filterTingkatChange}
						    value={this.state.filterTingkat}
						    options={[
						    	{
						    		key:'all',
						    		value:'all',
						    		text:'Semua'
						    	},
						    	{
						    		key:'X',
						    		value:'X',
						    		text:'X'
						    	},
						    	{
						    		key:'XI',
						    		value:'XI',
						    		text:'XI'
						    	},
						    	{
						    		key:'XII',
						    		value:'XII',
						    		text:'XII'
						    	}
						    ]}
						/>
					</Card>
					<Card style={{marginLeft:'60px'}} width="300px">
						<Header as="h4" color="blue">Program</Header>
						<Dropdown
						    placeholder='Select Friend'
						    fluid
						    selection
						    defaultValue="all"
						    onChange={this.filterProgramChange}
						    value={this.state.filterProgram}
						    options={[
						    	{
						    		key:'all',
						    		value:'all',
						    		text:'Semua'
						    	},
						    	{
						    		key:'mipa',
						    		value:'MIPA',
						    		text:'MIPA'
						    	},
						    	{
						    		key:'ips',
						    		value:'IPS',
						    		text:'IPS'
						    	},
						    	{
						    		key:'bahasa',
						    		value:'Bahasa',
						    		text:'Bahasa'
						    	}
						    ]}
						/>
					</Card>
				</Container>
				<ReactTable defaultPageSize={5} columns={this.columns} data={this.state.filterData}/>
			</Body>
		)
	}
}

export default BodyRekap
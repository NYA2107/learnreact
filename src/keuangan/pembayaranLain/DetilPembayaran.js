import React from 'react'
import styled from 'styled-components'
import {Header, Button, Icon, Dropdown, Input, Form, Radio} from 'semantic-ui-react'
import moment from 'moment'
import InputCurrency from '../../asset/InputCurrency'

let Container = styled.div`
	width:100%;
	display:flex;
`
let Card = styled.div`
	width:${props => props.width||'100%'};
	padding:10px;
`

class DetilPembyaran extends React.Component{
	constructor(props){
		super(props)
		this.state = {
			today:true,
			jenisPembayaran:'',
			jumlahPembayaran:0,
			waktuPembayaran:moment(),
			dateValue:''
		}
	}

	async componentDidMount(){
		if(this.props.waktuPembayaran){
			if(moment().format('YYYY-MM-DD') == this.props.waktuPembayaran.format('YYYY-MM-DD')){
				await this.setState({
					today:true
				})
			}else{
				await this.setState({
					today:false
				})
			}
		}
	}


	handleJenis=(e, {value}) => {
		// await this.setState({jenisPembayaran:value})
		this.props.onJenisChange(value)
	}

	handleJumlah=(data) => {
		// await this.setState({jumlahPembayaran:value})
		console.log(data.text)
		this.props.onJumlahChange(data.text)
	}

	handleRadio=async (e,{value})=>{
		// if(value){
		// 	await this.setState({waktuPembayaran:moment()})
		// }else{
		// 	await this.setState({waktuPembayaran:moment(this.state.dateValue)})
		// }
		await this.setState({today:value})
		this.props.onRadioChange(value)	
	}

	dateChange=(e,{value})=>{
		// console.log(value)
		// await this.setState({waktuPembayaran:moment(value), dateValue:value})
		this.props.onDateChange(moment(value))
	}

	render(){
		return(
			<Container>
				<Card width="300px">
					<Header as="h5" color="blue" style={{width:'auto'}}>Jenis Pembayaran</Header>
					<Dropdown
						disabled={this.props.disable}
						onChange={this.handleJenis}
						placeholder='Jenis'
						selection
						value={this.props.jenisPembayaran}
						options={[
							{
								key:1,
								text:'Kas Wajib',
								value:{
									key:1,
									text:'Kas Wajib',
									value:'kas'
								}
							},
							{
								key:2,
								text:'Iuran Mingguan',
								value:{
									key:2,
									text:'Iuran Mingguan',
									value:'mingguan'
								}
							}
						]}
					/>
				</Card>
				<Card width="300px">
					<Header as="h5" color="blue" style={{width:'auto'}}>Jumlah Pembayaran</Header>
					<InputCurrency disabled={this.props.disable} value={this.props.jumlahPembayaran || ''} onValueChange={this.handleJumlah}/>
				</Card>
				<Card width="300px">
					<Header as="h5" color="blue" style={{width:'auto'}}>Waktu Pembayaran</Header>
					<Form>
			    		<Form.Field>
					    	<Radio
					    		disabled={this.props.disable}
					    		label='Hari Ini'
					    		value={true}
					    		name="waktu"
					    		checked={this.props.today == true}
					    		onChange={this.handleRadio}
					    	/>
				    	</Form.Field>
				    	<Form.Field>
				    		<div style={{display:'inline-flex', alignItems:'center'}}>
						    	<Radio
						    		disabled={this.props.disable}
						    		value={false}
						    		name="waktu"
						    		checked={this.props.today == false}
						    		onChange={this.handleRadio}
						    	/>
						    	<Input
						    		type="date"
						    		disabled={this.props.today || this.props.disable}
						    		style={{marginLeft:'8px'}}
						    		size="mini"
						    		icon={{ name: 'calendar alternate'}}
								    labelPosition='right'
								    placeholder='Jumlah...'
								    onChange={this.dateChange}
								    value={this.props.waktuPembayaran.format('YYYY-MM-DD')}
								/>	
							</div>
					    </Form.Field>
				    </Form>
				</Card>

			</Container>
		)
	}
}

export default DetilPembyaran
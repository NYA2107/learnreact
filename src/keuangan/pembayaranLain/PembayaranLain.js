import React from 'react';
import HeaderC from './Header'
import Body from './Body'
import styled from 'styled-components'
import {Button, Modal, Input} from 'semantic-ui-react'
import ReactTable from 'react-table-v6'
import InputCurrency from '../../asset/InputCurrency'
import PageContainer from '../../asset/PageContainer'


// let Container = styled.div`
//   width:100%;
//   height:100vh;
//   padding:10px;
//   display:grid;
//   grid-template-rows:auto 1fr;
//   overflow:auto;
// `
// let HeaderFix = styled(HeaderC)`
//   grid-row:1/2;
// `

// let BodyFix = styled(Body)`
//   grid-row:2/3
// `

class InputSPP extends React.Component{

  constructor(props){
    super(props)
    this.state = {
      data:[],
      modalOpen:false
    }
    this.bodyRef = React.createRef()
    this.columns =  [
      {
        Header: 'Nama',
        accessor: 'name',
        width:100 // String-based value accessors!
      }, 
      {
        Header: 'NIS',
        accessor: 'number'
      }, 
      {
        Header: 'Kelas',
        accessor: 'classRoom'
      },
      {
        Header: 'Uang',
        accessor: 'jumlahPembayaran',
        Cell:props =>(<Input value={props.value}/>),
        width:180
      },
      {
        Header: 'Jenis',
        accessor: 'jenisPembayaran.text'
      },
      {
        Header: 'Tanggal',
        accessor: 'waktuPembayaran',
        Cell: props => (<Input
                    type="date"
                    size="mini"
                    icon={{ name: 'calendar alternate'}}
                    labelPosition='right'
                    placeholder='Jumlah...'
                    value={props.value}
                />),
        width:180
      },
      {
        Header: 'Catatan',
        accessor: 'catatan'
      },
      {
        Header: 'Aksi',
        accessor: 'aksi',
        Cell: props => <Button onClick={this.removeData} key={props.value} id={props.value}>x</Button>
      }
    ]
  }

  removeData=(e)=>{
    let data = this.bodyRef.current.state.selectedData
    let temp = this.state.data
    temp.splice(e.target.id,1)
    this.bodyRef.current.setState({selectedData:[]})
    this.setState({data:temp})
  }

  simpan = async () =>{
    let data = this.bodyRef.current.state.selectedData
    let temp = []
    console.log(data)
    data.forEach((v,i)=>{
      temp.push({
        name:v.name,
        number:v.number,
        classRoom:v.classRoom,
        jenisPembayaran:v.jenisPembayaran,
        jumlahPembayaran:v.jumlahPembayaran,
        waktuPembayaran:v.waktuPembayaran.format('YYYY-MM-DD'),
        catatan:'Nine',
        aksi:v.id
      })
    })
    // await this.setState({selectedData:this.bodyRef.current.state.selectedData})
    console.log(temp)
    await this.setState({data:temp,modalOpen:true})
  }

  onBatal=()=>{
    this.setState({modalOpen:false})
  }

  render(){
    return(
      <PageContainer>
        <HeaderC onSave={this.simpan}/>
        <Body ref={this.bodyRef} onDataChange={this.updateData}/>
        <Modal open={this.state.modalOpen}>
          <Modal.Header>Konfirmasi Pembayaran Lain-Lain</Modal.Header>
          <Modal.Content image scrolling>
            <ReactTable defaultPageSize={this.state.data.length} data={this.state.data} columns={this.columns} />
          </Modal.Content>
          <Modal.Actions>
            <Button onClick={this.onBatal}>Batal</Button>
            <Button>Simpan</Button>
          </Modal.Actions>
        </Modal>
      </PageContainer>  
    )
  }
}


export default InputSPP;
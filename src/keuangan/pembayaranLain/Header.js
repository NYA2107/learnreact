import React from 'react'
import styled from 'styled-components'
import {HeaderLayout , Left, Right} from '../../asset/HeaderContainer'
import {Header, Button, Icon} from 'semantic-ui-react'


class HeaderPembayaranLain extends React.Component{
	constructor(props){
		super(props)
	}
	
	simpan=()=>{
		this.props.onSave()
	}

	render(){
		return(
			<HeaderLayout>
				<Left>
					<Header as='h2'>
						Input Pembayaran Lain-Lain
						<Header.Subheader>
							Hari Ini : Selasa, 25 Juni 2019
						</Header.Subheader>
					</Header>
				</Left>
				<Right>
					<Button onClick={this.props.onSave} positive><Icon name='save' />Simpan</Button>
				</Right>
			</HeaderLayout>
		)
	}
}

export default HeaderPembayaranLain
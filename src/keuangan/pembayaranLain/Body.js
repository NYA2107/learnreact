import React from 'react'
import styled from 'styled-components'
import _ from 'lodash'
import moment from 'moment'
import Body from '../../asset/BodyContainer'
import {Search, Header, Button, Icon, Input, Divider, Card, Image, Segment, Grid, Radio, Form, Modal} from 'semantic-ui-react'
import GridContainer from '../../asset/GridContainer'
import GridContent from '../../asset/GridContent'
import CardList from '../../asset/CardList'
import DetilPembayaran from './DetilPembayaran'


let FlexDiv = styled.div`
	width:100%;
	display:flex;
	justify-content:flex-start;
	align-items:flex-start;
	align-content:flex-start;
`

class BodyPembayaranLain extends React.Component{

	constructor(props){
		super(props)
		this.initialState = { isLoading: false, results: [], value: '' }
		this.state = {
			isLoading:false,
			value:'',
			results:[],
			selectedId:[],
			selectedData:[],
			activeCard:null,
			detilView:{},
			today:true
		}
		this.data = [
			{
				id:1,
				name:'Alan Turing',
				date1:2,
				date2:6,
				number:28901108,
				classRoom:'X MIPA 1',
				imgSrc:'https://s3.amazonaws.com/uifaces/faces/twitter/stan/128.jpg',
				jenisPembayaran:'',
				jumlahPembayaran:'',
				waktuPembayaran:moment()
			},
			{
				id:2,
				name:'Robert Downey Jr',
				date1:6,
				date2:6,
				number:21072107,
				classRoom:'XII MIPA 1',
				imgSrc:'https://s3.amazonaws.com/uifaces/faces/twitter/ganserene/128.jpg',
				jenisPembayaran:'',
				jumlahPembayaran:'',
				waktuPembayaran:moment()
			}
		]
		this.source = [
			{
				"id":1,
		    	"title": "Alan Turing",
		    	"description": "28901108 / X MIPA 1",
		    	"image": "https://s3.amazonaws.com/uifaces/faces/twitter/stan/128.jpg"
		    },
		    {
		    	"id" : 2,
		    	"title": "Robert Downey Jr",
		    	"description": "21072107 / XII MIPA 1",
		    	"image": "https://s3.amazonaws.com/uifaces/faces/twitter/ganserene/128.jpg"
		    }
		]
	}

	kosongin=(value)=>{
		console.log(value)
		this.setState({selectedData:[]})
	}

	handleResultSelect = async (e, {result})=>{
		let tempSelected = this.state.selectedId
		tempSelected.push(result.id)
		await this.setState({selectedId:tempSelected})
		let tempData = []
		this.data.forEach((v,i)=>{
			if(this.state.selectedId.includes(v.id)){
				tempData.push(v)
			}
		})
		await this.setState({selectedData:tempData,value:''})
	}

	handleSearchChange = (e, { value }) => {
	    this.setState({ isLoading: true, value })

	    setTimeout(() => {
	      if (this.state.value.length < 1) return this.setState(this.initialState)

	      const re = new RegExp(_.escapeRegExp(this.state.value), 'i')
	      const isMatch = (result) => re.test(result.title)

	      this.setState({
	        isLoading: false,
	        results: _.filter(this.source, isMatch),
	      })
	    }, 300)
	}

	onCardClick = async (data) =>{
		let selectedIndex = this.state.selectedData.findIndex((el)=>el.id==data.id)
		if(this.state.selectedData[selectedIndex].waktuPembayaran.format('YYYY-MM-DD') == moment().format('YYYY-MM-DD')){
			await this.setState({today:true})
		}else{
			await this.setState({today:false})
		}
		await this.setState({activeCard:data.id})
		console.log(this.state.selectedData[this.state.selectedData.findIndex((el)=>el.id==data.id)])

	}

	onDetilChange = async (data) =>{
		let temp = []
		this.state.selectedData.forEach((v,i)=>{
			let val = Object.assign(v)
			if(v.id == data.id){
				temp.push(Object.assign(val,data))
			}else{
				temp.push(val)	
			}
		})

		await this.setState({selectedData:temp})

	}

	jenisChange=(value)=>{
		// let selectedIndex = this.state.selectedData.findIndex((el)=>el.id==this.state.activeCard)
		let temp = this.state.selectedData
		temp.forEach((v,i)=>{
			if(v.id == this.state.activeCard){
				Object.assign(v, {jenisPembayaran:value})
			}
		})

		this.setState({selectedData:temp})
	}

	jumlahChange=(value)=>{
		// let selectedIndex = this.state.selectedData.findIndex((el)=>el.id==this.state.activeCard)
		let temp = this.state.selectedData
		temp.forEach((v,i)=>{
			if(v.id == this.state.activeCard){
				Object.assign(v, {jumlahPembayaran:value})
			}
		})

		this.setState({selectedData:temp})
	}

	dateChange=(value)=>{
		let temp = this.state.selectedData
		temp.forEach((v,i)=>{
			if(v.id == this.state.activeCard){
				Object.assign(v, {waktuPembayaran:value})
			}
		})

		this.setState({selectedData:temp})

	}

	radioChange=(value)=>{
		console.log(value)
		let temp = this.state.selectedData
		if(value){
			temp.forEach((v,i)=>{
				if(v.id == this.state.activeCard){
					Object.assign(v, {waktuPembayaran:moment()})
				}
			})
		}
		this.setState({today:value,selectedData:temp})
	}

	getJenis(){
		let jenis = this.state.selectedData[this.state.selectedData.findIndex((el)=>el.id==this.state.activeCard)]
		if(jenis){
			return jenis.jenisPembayaran
		}else{
			return {}
		}
	}

	getJumlah(){

		let jumlah = this.state.selectedData[this.state.selectedData.findIndex((el)=>el.id==this.state.activeCard)]

		if(jumlah){
			console.log(jumlah)
			return jumlah.jumlahPembayaran
		}else{
			return null
		}
	}

	getDate(){
		let date = this.state.selectedData[this.state.selectedData.findIndex((el)=>el.id==this.state.activeCard)]
		if(date){
			return date.waktuPembayaran
		}else{
			return moment()
		}
	}

	render(){
		return(
			<Body>
				<Header as="h4" color="blue">Daftar Murid</Header>
				<div style={{width:'400px'}}>
					<Search
			            loading={this.state.isLoading}
			            onResultSelect={this.handleResultSelect}
			            onSearchChange={_.debounce(this.handleSearchChange, 500, {
			              leading: true,
			            })}
			            results={this.state.results}
			            value={this.state.value}
			            {...this.props}
			         />
				</div>
				<Divider/>
					<CardList activeCard={this.state.activeCard} onCardClick={this.onCardClick} data={this.state.selectedData}/>
				<Divider horizontal>
			    	<Header as="h4" color="blue">Detail Pembayaran</Header>
			    </Divider>
			    <DetilPembayaran
			    	disable={this.state.activeCard == null}
			    	id={this.state.activeCard}
			    	jenisPembayaran={this.getJenis()}
			    	jumlahPembayaran={this.getJumlah()}
				    waktuPembayaran={this.getDate()} 
				    onDataChange={this.onDetilChange}
				    onJenisChange={this.jenisChange}
				    onJumlahChange={this.jumlahChange}
				    onDateChange={this.dateChange}
				    onRadioChange={this.radioChange}
				    today={this.state.today}
			    />
			</Body>
		)
	}
}

export default BodyPembayaranLain
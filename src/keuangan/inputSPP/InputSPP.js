import React from 'react';
import Header from './Header'
import Body from './Body'
import styled from 'styled-components'

let Container = styled.div`
  width:100%;
  height:100vh;
  padding:10px;
  display:grid;
  grid-template-rows:auto 1fr;
  overflow:auto;
`
let HeaderFix = styled(Header)`
  grid-row:1/2;
`

let BodyFix = styled(Body)`
  grid-row:2/3
`

class InputSPP extends React.Component{

  constructor(props){
    super(props)
    this.state = {
      data:[
        {
          name:'imam'
        }
      ]
    }
  }

  simpan=()=>{
    console.log(this.state.data)
  }

  updateData=()=>{
    
  }

  render(){
    return(
      <Container>
        <HeaderFix onSave={this.simpan}/>
        <BodyFix onDataChange={this.updateData}/>
      </Container>  
    )
  }
}


export default InputSPP;
import React from 'react'
import styled from 'styled-components'
import Body from '../../asset/BodyContainer'
import {Header, Button, Icon, Input, Divider, Card, Image, Segment, Grid, Radio, Form, Modal} from 'semantic-ui-react'
import GridContainer from '../../asset/GridContainer'
import GridContent from '../../asset/GridContent'
import CardList from '../../asset/CardList'
import InputCurrency from '../../asset/InputCurrency'


let FlexDiv = styled.div`
	width:100%;
	display:flex;
	justify-content:flex-start;
	align-items:flex-start;
	align-content:flex-start;
`

class BodySPP extends React.Component{
	users = [
		{
			id:1,
			name:'Alan Turing',
			date1:2,
			date2:6,
			number:28901108,
			classRoom:'X MIPA 1',
			imgSrc:'https://react.semantic-ui.com/images/avatar/large/molly.png'
		},
		{
			id:2,
			name:'Robert Downey Jr',
			date1:6,
			date2:6,
			number:21072107,
			classRoom:'XII MIPA 1',
			imgSrc:'https://react.semantic-ui.com/images/avatar/large/matthew.png'
		}
	]	

	constructor(props){
		super(props)

		this.state = {
			data : this.users,
			dataFilter : [],
			today:true,
			modalOpen:true
		}
	}

	componentDidMount(){
		this.setState({dataFilter:this.users})
	}

	filter(data, search){
		return data.filter((v)=>{
			return (v.name.includes(search) || v.number.toString().includes(search))
		})
	}

	searchChange=(e)=>{
		this.setState({dataFilter:this.filter(this.state.data, e.target.value)})
	}

	handleRadio=(e,{value})=>{
		console.log(value)
		this.setState({today:value})
	}

	handleOpen = () => this.setState({ modalOpen: true })

	handleClose = () => this.setState({ modalOpen: false })	

	currChange = (val) =>{
		console.log(val)
	}

	render(){
		return(
			<Body>
				<Header as="h4" color="blue">Daftar Murid</Header>
				<div style={{width:'400px'}}>
					<Input fluid size="small" icon='search' iconPosition='left' onChange={this.searchChange} placeholder='Cari Nama/NIS Murid (minimal 3 karakter)' />
				</div>
				<Divider/>
					<CardList data={this.state.dataFilter}/>
				<Divider horizontal>
			      <Header as="h4" color="blue">Detail Pembayaran</Header>
			    </Divider>
			    <FlexDiv>
				    <div style={{ width:'300px'}}>
				    	<Header as="h5" color="blue" style={{width:'auto'}}>Jumlah Pembayaran</Header>
				    	<Input
				    		size="mini"
						    label={{ basic: true, content: 'Rp' }}
						    labelPosition='left'
						    placeholder='Jumlah...'
						/>
						<InputCurrency onValueChange={this.currChange} value={1234000}/>
				    </div>
				    <div style={{marginLeft:'10px'}}>
				    	<Header as="h5" color="blue">Waktu Pembayaran</Header>
				    	<Form>
				    		<Form.Field>
						    	<Radio
						    		label='Hari Ini'
						    		value={true}
						    		name="waktu"
						    		checked={this.state.today === true}
						    		onChange={this.handleRadio}
						    	/>
					    	</Form.Field>
					    	<Form.Field>
					    		<div style={{display:'inline-flex', alignItems:'center'}}>
							    	<Radio
							    		value={false}
							    		name="waktu"
							    		checked={this.state.today === false}
							    		onChange={this.handleRadio}
							    	/>
							    	<Input
							    		type="date"
							    		disabled={this.state.today}
							    		style={{marginLeft:'8px'}}
							    		size="mini"
							    		icon={{ name: 'calendar alternate'}}
									    labelPosition='right'
									    placeholder='Jumlah...'
									/>	
								</div>
						    </Form.Field>
					    </Form>

				    </div>
				</FlexDiv>
				<Modal
			        open={this.state.modalOpen}
			        onClose={this.handleClose}
			        basic
			        size='small'
			      >
			        <Header icon='browser' content='Cookies policy' />
			        <Modal.Content>
			          <h3>This website uses cookies to ensure the best user experience.</h3>
			        </Modal.Content>
			        <Modal.Actions>
			          <Button color='green' onClick={this.handleClose} inverted>
			            <Icon name='checkmark' /> Got it
			          </Button>
			        </Modal.Actions>
			      </Modal>
			</Body>
		)
	}
}

export default BodySPP
import React from 'react';
import Header from './keuangan/inputSPP/Header'
import Body from './keuangan/inputSPP/Body'
import styled from 'styled-components'
import InputSPP from './keuangan/inputSPP/InputSPP'
import PembayaranLain from './keuangan/pembayaranLain/PembayaranLain'
import KEU3DaftarKelas from './keuangan/rekapPembayaran/KEU3DaftarKelas'
import KEU3Rekap from './keuangan/rekapPembayaran/Rekap/KEU3Rekap'
import KEU3RekapIndividu from './keuangan/rekapPembayaran/RekapIndividu/KEU3Rekap'
import Temp from './keuangan/Temp'

function App() {
  return (
    <div>
      <KEU3RekapIndividu/>
    </div>
  );
}

export default App;
